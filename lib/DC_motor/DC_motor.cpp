//Autor: Cezary Wernik
#include <DC_motor.h>

DC_motor::DC_motor(int _A_pin, int _B_pin)
{
  A = _A_pin;
  B = _B_pin;
}

void DC_motor::init()
{
  pinMode(A,OUTPUT);
  pinMode(B,OUTPUT);
}

void DC_motor::runForward()
{
  digitalWrite(A,HIGH);
  digitalWrite(B,LOW);
}

void DC_motor::runBackward()
{
  digitalWrite(A,LOW);
  digitalWrite(B,HIGH);
}

void DC_motor::stop()
{
  digitalWrite(A,LOW);
  digitalWrite(B,LOW);
}
