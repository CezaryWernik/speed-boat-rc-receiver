//Autor: Cezary Wernik

#ifndef DC_motor_h
#define DC_motor_h

#include <Arduino.h>
#include <stdint.h>

class DC_motor
{
private:
  int A;
  int B;
public:
  DC_motor(int _A_pin, int _B_pin);
  void init();
  void runForward();
  void runBackward();
  void stop();
};

#endif
