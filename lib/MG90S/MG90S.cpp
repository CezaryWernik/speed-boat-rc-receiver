//Autor: Cezary Wernik

#include <MG90S.h>

MG90S::MG90S(int _S_pin)
{
  S = _S_pin;
}

void MG90S::init()
{
  pinMode(S, OUTPUT);
  setAngle(90);
}

void MG90S::setAngle(int angle)
{
  int pulseAngle=map(angle,0,180,pulseMinLen,pulseMaxLen);//konwersja miary kątowej na czas impulsu dla serwa
  noInterrupts();//na chwilę wyłaczam przerwania aby generowane impulsy dla serwa były właściwych długości
  digitalWrite(S, HIGH);
  delayMicroseconds(pulseAngle);
  digitalWrite(S, LOW);
  delayMicroseconds(impulsePeriod - pulseAngle);//odczekanie właściwej długości zanim zostanie wysłany kolejny impuls ustawiający
  //dodatkowo dodany czas :
  delayMicroseconds(180*impulsePeriod); //na przemieszczenie, trochę na zapas za dużo ale lepiej więcej niż mniej
  interrupts();
}
