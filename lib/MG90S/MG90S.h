//Autor: Cezary Wernik

#ifndef MG90S_h
#define MG90S_h

#include <Arduino.h>
#include <stdint.h>

class MG90S
{
private:
  int impulsePeriod = 20000; //us
  int pulseMinLen = 544;   //us
  int pulseMaxLen = 2400;   //us
  int S;
public:
  MG90S(int _S_pin);
  void init();
  void setAngle(int angle);
};

#endif
