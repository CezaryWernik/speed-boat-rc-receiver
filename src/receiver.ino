//wszystko dla modułu nrf
#include <SPI.h>
#include <RH_NRF24.h>
#define CE 9 //digital 9
#define CSN 10 //digital 10
RH_NRF24 nrf24(CE, CSN);
uint8_t joystickState[6]; // { '[', 'X', 'Y', 'C', ']', '\0' };
//można też użyc długości [RH_NRF24_MAX_MESSAGE_LEN]
//joystickState jest tak na prawdę buforem odbioru
#define disconectionTimeLimit 1000 //ms
int disconnectionCounter = 0;


//wszystko dla serwa
#include <MG90S.h>
#define sPin 8
MG90S serwo(sPin);

//wszystko dla motoru DC
#include <DC_motor.h>
#define aPin 7
#define bPin 6
DC_motor motor(aPin, bPin);

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  ; // czekaj aż serial port zostanie zainicjowany, potrzebne tylko dla Arduino Leonardo
  if (!nrf24.init())
  Serial.println("init failed");

  //niezbędne ustawienia dla modułu nrf24, wybieram kanał 1  
  if (!nrf24.setChannel(1))
  Serial.println("setChannel failed");
  if (!nrf24.setRF(RH_NRF24::DataRate2Mbps, RH_NRF24::TransmitPower0dBm))
  Serial.println("setRF failed");

  serwo.init();
  motor.init();
}


void loop()
{
  if (nrf24.available())
  {
    //gdy jest łącznośc
    if (nrf24.recv(joystickState, sizeof(joystickState)))
    {
      //sprawdzam poprawnośc przesłanej "paczki"
      if(joystickState[0]=='[' && joystickState[4]==']')
      {
        //resetuję licznik braku łączności
        disconnectionCounter = 0;

        //obsługa osi X - lewo/prawo
        switch (joystickState[1]) {
          case 'a':
          serwo.setAngle(45);
          break;
          case 'b':
          serwo.setAngle(90);
          break;
          case 'c':
          serwo.setAngle(135);
          break;
        }

        //obsługa osi Y - przód/tył
        switch (joystickState[2]) {
          case 'a':
          motor.runBackward();
          break;
          case 'b':
          motor.stop();
          break;
          case 'c':
          motor.runForward();
          break;
        }

        if(joystickState[3]=='c')
        {
          //wykonaj akcję kliknięcia
          //tu do dobrego działania wymagane jest zaimplementowanie czegoś w rodzaju "debouncingu"
        }
      }
    }
  }else{
    //gdy łaczności nie ma
    delay(1);
    disconnectionCounter++;
    if(disconnectionCounter>=disconectionTimeLimit)
    {
      serwo.setAngle(90);
      motor.stop();
    }
  }
}
